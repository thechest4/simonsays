// Fill out your copyright notice in the Description page of Project Settings.

#include "SimonSaysGameState.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/GameMode.h"
#include "UI/PatternDisplayWidget.h"
#include "UI/GameOverWidget.h"
#include "UI/GameStateMessageWidget.h"
#include "UI/CountdownTimerWidget.h"
#include "GameFramework/PlayerController.h"



void ASimonSaysGameState::ShowPatternDisplayWidget_Implementation()
{
	if (PatternDisplayWidget == nullptr)
	{
		if (PatternDisplayWidgetClass != nullptr)
		{
			PatternDisplayWidget = CreateWidget<UPatternDisplayWidget>(GetWorld(), PatternDisplayWidgetClass);
			PatternDisplayWidget->InitPatternDisplayWidget();
		}
	}

	if (PatternDisplayWidget != nullptr)
	{
		PatternDisplayWidget->AddToViewport();
	}
}

void ASimonSaysGameState::HidePatternDisplayWidget_Implementation()
{
	PatternDisplayWidget->RemoveFromParent();
}

void ASimonSaysGameState::AddPatternElement_Implementation(int argElementId)
{
	PatternDisplayWidget->AddToPattern(argElementId);
}

void ASimonSaysGameState::ShowResultForSelection_Implementation(int argPatternId, bool argbResult)
{
	PatternDisplayWidget->ShowResultForElement(argPatternId, argbResult);
}

void ASimonSaysGameState::ResetAllResults_Implementation()
{
	PatternDisplayWidget->ResetAllElementResults();
}

void ASimonSaysGameState::ToggleAllElementsHidden_Implementation(bool argbIsHidden)
{
	PatternDisplayWidget->ToggleAllElementsHidden(argbIsHidden);
}

void ASimonSaysGameState::SetPatternElements_Implementation(const TArray<int>& argElementIds)
{
	PatternDisplayWidget->DisplayPattern(argElementIds);
}

void ASimonSaysGameState::ShowGameOverScreen_Implementation(int argWinnerIndex)
{
	if (GameOverWidget == nullptr)
	{
		GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(), GameOverWidgetClass);
	}

	GameOverWidget->AddToViewport();
	GameOverWidget->SetGameOverScreenText(argWinnerIndex);
}

void ASimonSaysGameState::HideGameOverScreen_Implementation()
{
	GameOverWidget->RemoveFromParent();
}

void ASimonSaysGameState::ClearPattern_Implementation()
{
	PatternDisplayWidget->ClearPattern();
}

void ASimonSaysGameState::ShowCurrentTurnMessage_Implementation(int CurrentPlayerStateId, float MessageDuration)
{
	if (CurrentTurnMessageWidget == nullptr)
	{
		CurrentTurnMessageWidget = CreateWidget<UGameStateMessageWidget>(GetWorld(), CurrentTurnMessageWidgetClass);
		CurrentTurnMessageWidget->InitGameStateMessageWidget();
	}

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	FString CurrentPlayerName = "Your";

	if (PC->PlayerState->PlayerId != CurrentPlayerStateId)
	{
		for (int i = 0; i < PlayerArray.Num(); i++)
		{
			if (PlayerArray[i]->PlayerId == CurrentPlayerStateId)
			{
				CurrentPlayerName = PlayerArray[i]->PlayerName + "'s";
				break;
			}
		}
	}

 	FString MessageStr = CurrentPlayerName + " turn";
 	CurrentTurnMessageWidget->DisplayMessage(MessageStr, 3.0f);
}

void ASimonSaysGameState::ShowTurnResultMessage_Implementation(int CurrentPlayerStateId, bool bTurnWasSuccessful, float MessageDuration)
{
	if (TurnResultMessageWidget == nullptr)
	{
		TurnResultMessageWidget = CreateWidget<UGameStateMessageWidget>(GetWorld(), TurnResultMessageWidgetClass);
		TurnResultMessageWidget->InitGameStateMessageWidget();
	}

	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	bool bWasMyTurn = (PC->PlayerState->PlayerId == CurrentPlayerStateId);
	FString CurrentPlayerName = "You";

	if (!bWasMyTurn)
	{
		for (int i = 0; i < PlayerArray.Num(); i++)
		{
			if (PlayerArray[i]->PlayerId == CurrentPlayerStateId)
			{
				CurrentPlayerName = PlayerArray[i]->PlayerName;
				break;
			}
		}
	}

	FString TurnResultStr = CurrentPlayerName;
	if (bTurnWasSuccessful)
	{
		TurnResultStr += (bWasMyTurn) ? " live...  for now" : " lives... unfortunately";
	}
	else
	{
		TurnResultStr += (bWasMyTurn) ? " have died." : " has died.";
	}
	
	TurnResultMessageWidget->DisplayMessage(TurnResultStr, 3.0f);
}

void ASimonSaysGameState::HideAllGameStateMessages_Implementation()
{
	if (CurrentTurnMessageWidget != nullptr)
	{
		CurrentTurnMessageWidget->ShutDownWidget();
	}

	if (TurnResultMessageWidget != nullptr)
	{
		TurnResultMessageWidget->ShutDownWidget();
	}

	if (CountdownTimerWidget != nullptr)
	{
		CountdownTimerWidget->ShutDownWidget();
	}
}

void ASimonSaysGameState::ShowCountdownTimer_Implementation(float argDuration)
{
	if (CountdownTimerWidget == nullptr)
	{
		CountdownTimerWidget = CreateWidget<UCountdownTimerWidget>(GetWorld(), CountdownTimerWidgetClass);
		CountdownTimerWidget->InitCountdownTimerWidget();
	}

	CountdownTimerWidget->StartTimer(argDuration);
}
