// Fill out your copyright notice in the Description page of Project Settings.

#include "SimonSaysGameModeBase.h"
#include "SimonSaysGameState.h"
#include "GameFramework/GameMode.h"
#include "SimonSaysPlayerController.h"
#include "SimonSaysPlayerState.h"
#include "Engine/DataTable.h"
#include "PatternElement.h"
#include "GameFramework/PlayerState.h"



ASimonSaysGameModeBase::ASimonSaysGameModeBase()
{
	bDelayedStart = true;
	//Try seamless again later
	//bUseSeamlessTravel = true;
}

void ASimonSaysGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	//If this is the first player to connect, initialize the match state
	if (SSGameState == nullptr)
	{
		SSGameState = Cast<ASimonSaysGameState>(GameState);
		if (SSGameState != nullptr)
		{
			SSGameState->SetMatchState(MatchState::WaitingToStart);
		}
	}

	if (SSGameState->GetMatchState() == MatchState::WaitingToStart)
	{
		ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(NewPlayer);
		InitializePlayer(SSPC, true);
	}
	else //If we're not waiting to start then we're not accepting new players
	{
		NewPlayer->ClientReturnToMainMenu(FString(TEXT("Game is already in progress")));
	}
}

void ASimonSaysGameModeBase::AdvanceIfAllPlayersReady()
{
	bool bAreAllPlayersReady = true;

	//Check if all players are ready to start game
	for (int i = 0; i < GameState->PlayerArray.Num(); i++)
	{
		ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[i]);
		if (!SSPS->bIsReady)
		{
			bAreAllPlayersReady = false;
			break;
		}
	}

	if (bAreAllPlayersReady && GameState->PlayerArray.Num() >= RequiredPlayerCount)
	{
		SSGameState->SetMatchState(MatchState::InProgress);

		for (int i = 0; i < GameState->PlayerArray.Num(); i++)
		{	
			ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[i]);
			//Bring all players to life!
			SSPS->ServerSetIsAlive(true);

			ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[i]->GetOwner());
			//Hide all ReadyUp widgets 
			SSPC->HidePlayerReadyWidget();
			//Hide the Ready/NotReady status and instead show the Alive/Dead status in the PlayerStatus widget
			SSPC->ShowSurvivalPlayerStatus();
		}

		//Show the Pattern Display Widget on all clients
		SSGameState->ShowPatternDisplayWidget();
		
		//Start the game loop
		AdvanceToNextTurn(true, false);
	}
}

void ASimonSaysGameModeBase::ReceivePlayerElementSubmission(int argPatternIndex, int argElementId)
{
	if (argPatternIndex >= 0 && argPatternIndex < CurrentPatternIds.Num())
	{
		bool isGameEnding = false;
		bool bWasCorrectSubmission = (CurrentPatternIds[argPatternIndex] == argElementId);
		SSGameState->ShowResultForSelection(argPatternIndex, bWasCorrectSubmission);

		//Either player has just correctly submitted the last pattern element, OR the player has submitted an incorrect element
		if ((bWasCorrectSubmission && argPatternIndex >= CurrentPatternIds.Num() - 1) || !bWasCorrectSubmission)
		{
			SSGameState->ShowTurnResultMessage(CurrentPlayerStateId, bWasCorrectSubmission, 3.0f);
			//If the submission was incorrect, kill the current player
			if (!bWasCorrectSubmission)
			{
				ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[CurrentPlayerIndex]);
				SSPS->ServerSetIsAlive(false);
				isGameEnding = IsGameOver();
			}

			if (!isGameEnding)
			{
				EndPlayForCurrentPlayer();

				//We set a timer here so that all players have the opportunity to see whether the currently player guessed correctly or not
				FTimerHandle EndTurnDelayHandle;
				FTimerDelegate AdvanceToNextTurnDelegate;
				AdvanceToNextTurnDelegate.BindUFunction(this, FName("AdvanceToNextTurn"), false, false);
				GetWorldTimerManager().SetTimer(EndTurnDelayHandle, AdvanceToNextTurnDelegate, 1.0f, false, DelayBetweenTurns);
			}
			else
			{
				EndGame();
			}
		}
	}
}

void ASimonSaysGameModeBase::StartPlayForCurrentPlayer()
{
	//Hide all element colors
	SSGameState->ToggleAllElementsHidden(true);
	SSGameState->ShowCurrentTurnMessage(CurrentPlayerStateId, 3.0f);

	ASimonSaysPlayerController* CurrentPlayer = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[CurrentPlayerIndex]->GetOwner());
	if (CurrentPlayer != nullptr)
	{
		CurrentPlayer->ShowPlayerElementTray();
	}
}

void ASimonSaysGameModeBase::EndPlayForCurrentPlayer()
{
	//Show all element colors
	SSGameState->ToggleAllElementsHidden(false);

	ASimonSaysPlayerController* CurrentPlayer = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[CurrentPlayerIndex]->GetOwner());
	if (CurrentPlayer != nullptr)
	{
		CurrentPlayer->HidePlayerElementTray();
	}
}

int ASimonSaysGameModeBase::DeterminePlayerIndexToStart()
{
	return FMath::RandHelper(GameState->PlayerArray.Num());
}

void ASimonSaysGameModeBase::IncrementCurrentPlayerIndex()
{
	CurrentPlayerIndex++;
	if (CurrentPlayerIndex >= GetNumPlayers())
	{
		CurrentPlayerIndex = 0;
	}

	//If the next selected player is already out, select a new one!
	ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[CurrentPlayerIndex]);
	if (!SSPS->bIsAlive)
	{
		IncrementCurrentPlayerIndex();
	}
}

int ASimonSaysGameModeBase::GetRandomElementId()
{
	int ToReturn = 0;

	FString ContextString;
	TArray<FPatternElementData*> PatternElements;
	ElementDataTable->GetAllRows(ContextString, PatternElements);

	int RandomIndex = FMath::RandHelper(PatternElements.Num());

	for (int i = 0; i < PatternElements.Num(); i++)
	{
		if (i == RandomIndex)
		{
			ToReturn = PatternElements[i]->ElementId;
			break;
		}
	}
	return ToReturn;
}

void ASimonSaysGameModeBase::AdvancePattern()
{
	if (CurrentPatternIds.Num() < PatternElementLimit)
	{
		CurrentShowPatternDuration = ShowPatternDuration;
		int NewElementId = GetRandomElementId();
		CurrentPatternIds.Add(NewElementId);
		SSGameState->AddPatternElement(NewElementId);
	}
	else //if we have reached the PatternElementLimit then randomize the whole pattern instead of adding a new element
	{
		CurrentShowPatternDuration = ShowPatternDurationSuddenDeath;
		for (int i = 0; i < PatternElementLimit; i++)
		{
			CurrentPatternIds[i] = GetRandomElementId();
		}

		SSGameState->SetPatternElements(CurrentPatternIds);
		//Update all element colors after setting them
		SSGameState->ToggleAllElementsHidden(false);
	}
}

void ASimonSaysGameModeBase::AdvanceToNextTurn(bool bIsFirstTurn, bool bIsDueToDisconnection)
{
	//Show all element colors.  This is a redundant call under normal circumstances, but handles the case of a disconnected player
	SSGameState->ToggleAllElementsHidden(false);

	SSGameState->ResetAllResults();
	AdvancePattern();

	if (bIsFirstTurn)
	{
		CurrentPlayerIndex = DeterminePlayerIndexToStart();
	}
	//If the current player disconnected, we don't need to increment the player index since the next player will fall down into the current index slot.  
	//We do need to wrap the index if it was the last player though
	else if (bIsDueToDisconnection)
	{
		if (CurrentPlayerIndex >= GetNumPlayers())
		{
			CurrentPlayerIndex = 0;
		}
	}
	else
	{
		IncrementCurrentPlayerIndex();
	}

	//This is the PlayerId stored in the PlayerState that represents the current player
	CurrentPlayerStateId = GameState->PlayerArray[CurrentPlayerIndex]->PlayerId;
	//Update current player indicator on all clients
	for (int i = 0; i < GameState->PlayerArray.Num(); i++)
	{
		ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[i]->GetOwner());
		SSPC->UpdateCurrentPlayerStatus(CurrentPlayerStateId);
	}

	//We set a timer here to display the pattern for a certain amount of time before allowing the player to submit guesses
	FTimerHandle ShowPatternDurationHandle;
	GetWorldTimerManager().SetTimer(ShowPatternDurationHandle, this, &ASimonSaysGameModeBase::StartPlayForCurrentPlayer, 1.0f, false, CurrentShowPatternDuration);
	SSGameState->ShowCountdownTimer(CurrentShowPatternDuration);
}

bool ASimonSaysGameModeBase::IsGameOver()
{
	int NumPlayersStillAlive = 0;

	for (int i = 0; i < GameState->PlayerArray.Num(); i++)
	{
		ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[i]);
		if (SSPS->bIsAlive)
		{
			NumPlayersStillAlive++;
		}
	}

	return (NumPlayersStillAlive <= 1);
}

void ASimonSaysGameModeBase::EndGame()
{
	SSGameState->SetMatchState(MatchState::WaitingPostMatch);

	//Clear any timers we've currently got going.
	GetWorldTimerManager().ClearAllTimersForObject(this);

	int WinnerPlayerId = 0;

	for (int i = 0; i < GameState->PlayerArray.Num(); i++)
	{
		if (GameState->PlayerArray[i] != nullptr && !GameState->PlayerArray[i]->IsActorBeingDestroyed())
		{
			ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[i]->GetOwner());
			SSPC->HideAllWidgets();

			ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[i]);
			if (SSPS->bIsAlive)
			{
				WinnerPlayerId = SSPS->PlayerId;
			}
		}
	}

	SSGameState->HideAllGameStateMessages();
	SSGameState->HidePatternDisplayWidget();
	SSGameState->ShowGameOverScreen(WinnerPlayerId);
}

void ASimonSaysGameModeBase::InitializePlayer(ASimonSaysPlayerController* PlayerToInitialize, bool bSetDefaultName)
{
	//Initialize widgets for the new player
	PlayerToInitialize->ShowPlayerReadyWidgets();
	PlayerToInitialize->ShowPlayerStatusWidget();

	if (bSetDefaultName)
	{
		FString NewPlayerName = FString::Printf(TEXT("%s%i"), *DefaultPlayerName.ToString(), PlayerToInitialize->GetUniqueID());
		PlayerToInitialize->PlayerState->SetPlayerName(NewPlayerName);
	}
}

void ASimonSaysGameModeBase::ReloadCurrentLevel()
{
	FString CurrentMapName = GetWorld()->GetMapName();
	GetWorld()->ServerTravel(FString::Printf(TEXT("%s?listen"), *CurrentMapName));
}

void ASimonSaysGameModeBase::RestartGame()
{
	//Hide GameOver screen
	SSGameState->HideGameOverScreen();

	//Re initialize the MatchState		
	SSGameState->SetMatchState(MatchState::WaitingToStart);
	
	//Clear pattern from last game
	CurrentPatternIds.Empty();
	SSGameState->ClearPattern();
	
	//Re initialize all remaining players
	for (int i = 0; i < GameState->PlayerArray.Num(); i++)
	{
		ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GameState->PlayerArray[i]->GetOwner());
		InitializePlayer(SSPC, false);
		//Resets the PlayerStatus window to show ready status again instead of survival status
		SSPC->ShowReadyPlayerStatus();

		ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(GameState->PlayerArray[i]);
		SSPS->ServerSetIsReady(false);
	}
}

void ASimonSaysGameModeBase::RemovePlayerState(APlayerState* argPlayerStateToRemove)
{
	//Since we aren't using seamless travel we don't have to worry about handling disconnections if we're about to travel to a new map
	if (SSGameState->GetMatchState() != MatchState::LeavingMap)
	{
		//We cache the playerid for the disconnected player here so that we can tell posthumously whether it was the current player or not
		int CachedDisconnectedPlayerId = argPlayerStateToRemove->PlayerId;

		ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(argPlayerStateToRemove);
		SSPS->DestroyPlayerState();

		//Check if advancement is necessary due to player disconnect
		if (SSGameState->GetMatchState() == MatchState::WaitingToStart)
		{
			AdvanceIfAllPlayersReady();
		}
		else if (SSGameState->GetMatchState() == MatchState::InProgress)
		{
			if (IsGameOver())
			{
				EndGame();
			}
			//Was it the current player who disconnected?
			else if(CurrentPlayerStateId == CachedDisconnectedPlayerId)
			{
				SSGameState->HideAllGameStateMessages();
				AdvanceToNextTurn(false, true);
			}
		}
	}
}
