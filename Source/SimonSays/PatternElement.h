// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PatternElement.generated.h"

USTRUCT(BlueprintType)
struct FPatternElementData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ElementId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor ElementColor;
};