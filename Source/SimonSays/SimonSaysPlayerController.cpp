// Fill out your copyright notice in the Description page of Project Settings.

#include "SimonSaysPlayerController.h"
#include "UI/ReadyUpWidget.h"
#include "UI/PlayerStatusWidget.h"
#include "GameFramework/GameStateBase.h"
#include "UI/PlayerElementTray.h"
#include "SimonSaysGameModeBase.h"




ASimonSaysPlayerController::ASimonSaysPlayerController() :Super()
{
	bShowMouseCursor = true;
}

void ASimonSaysPlayerController::SubmitElementSelection_Implementation(int argPatternIndex, int argElementId)
{
	ASimonSaysGameModeBase* SSGM = Cast<ASimonSaysGameModeBase>(GetWorld()->GetAuthGameMode());
	SSGM->ReceivePlayerElementSubmission(argPatternIndex, argElementId);
}

bool ASimonSaysPlayerController::SubmitElementSelection_Validate(int argPatternIndex, int argElementId)
{
	return true;
}

void ASimonSaysPlayerController::ShowPlayerReadyWidgets_Implementation()
{
	if (ReadyUpWidgetClass != nullptr)
	{
		PlayerReadyWidget = CreateWidget<UReadyUpWidget>(GetWorld(), ReadyUpWidgetClass);
	}

	if (PlayerReadyWidget != nullptr)
	{
		PlayerReadyWidget->AddToViewport();
		PlayerReadyWidget->SetOwningPlayerController(this);
	}
}

void ASimonSaysPlayerController::ShowPlayerStatusWidget_Implementation()
{	
	if (PlayerStatusWidgetClass != nullptr)
	{
		if (PlayerStatusWidget == nullptr)
		{
			PlayerStatusWidget = CreateWidget<UPlayerStatusWidget>(GetWorld(), PlayerStatusWidgetClass);
			PlayerStatusWidget->InitPlayerStatusWidget();

			//The server must add its PlayerState here since it will not be able to use replication notifies as an entry point like clients do
			if (Role == ROLE_Authority)
			{
				AddPlayerStatus(PlayerState);
			}
		}

		PlayerStatusWidget->AddToViewport();
	}
}

void ASimonSaysPlayerController::ShowPlayerElementTray_Implementation()
{
	if (PlayerElementTray == nullptr)
	{
		if (PlayerElementTrayClass != nullptr)
		{
			PlayerElementTray = CreateWidget<UPlayerElementTray>(GetWorld(), PlayerElementTrayClass);
			PlayerElementTray->SetOwningPlayerController(this);
			PlayerElementTray->SetUpElements();
		}
	}

	if (PlayerElementTray != nullptr)
	{
		PlayerElementTray->AddToViewport();
		PlayerElementTray->ResetPatternIndex();
	}
}

void ASimonSaysPlayerController::HidePlayerElementTray_Implementation()
{
	if (PlayerElementTray != nullptr)
	{
		PlayerElementTray->RemoveFromParent();
	}
}

void ASimonSaysPlayerController::HidePlayerReadyWidget_Implementation()
{
	if (PlayerReadyWidget != nullptr)
	{
		PlayerReadyWidget->RemoveFromParent();
	}
}

void ASimonSaysPlayerController::ShowSurvivalPlayerStatus_Implementation()
{
	if (PlayerStatusWidget != nullptr)
	{
		PlayerStatusWidget->SetGameAppearanceOnAllRows();
	}
}

void ASimonSaysPlayerController::ShowReadyPlayerStatus_Implementation()
{
	if (PlayerStatusWidget != nullptr)
	{
		PlayerStatusWidget->SetPreGameAppearanceOnAllRows();
	}
}

void ASimonSaysPlayerController::UpdateCurrentPlayerStatus_Implementation(int argCurrentPlayerIndex)
{
	if (PlayerStatusWidget != nullptr)
	{
		PlayerStatusWidget->ToggleCurrentPlayerIndicatorOnAllRows(argCurrentPlayerIndex);
	}
}

void ASimonSaysPlayerController::HideAllWidgets_Implementation()
{
	//It is actually possible to win without ever seeing the PlayerElementTray, so we need this null check
	if (PlayerElementTray != nullptr)
	{
		PlayerElementTray->RemoveFromParent();
	}
	
	if (PlayerStatusWidget != nullptr)
	{
		PlayerStatusWidget->RemoveFromParent();
	}
}

void ASimonSaysPlayerController::ResetGame_Implementation()
{
	if (Role == ROLE_Authority)
	{
		ASimonSaysGameModeBase* SSGM = Cast<ASimonSaysGameModeBase>(GetWorld()->GetAuthGameMode());
		//SSGM->ReloadCurrentLevel();
		SSGM->RestartGame();
	}
}

bool ASimonSaysPlayerController::ResetGame_Validate()
{
	return true;
}

void ASimonSaysPlayerController::AddPlayerStatus(APlayerState* argPlayerState)
{
	if (PlayerStatusWidget != nullptr)
	{
		PlayerStatusWidget->AddRow(argPlayerState);
	}
}
