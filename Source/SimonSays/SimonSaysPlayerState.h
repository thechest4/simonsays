// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SimonSaysPlayerState.generated.h"

class ASimonSaysPlayerController;

UCLASS()
class SIMONSAYS_API ASimonSaysPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	ASimonSaysPlayerState();

	UPROPERTY(BlueprintReadWrite, Replicated)
	bool bIsAlive = false;

	UPROPERTY(BlueprintReadWrite, Replicated)
	bool bIsReady = false;

	UFUNCTION()
	virtual void OnRep_PlayerName() override;

	virtual void OnDeactivated() override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetPlayerName(const FString& argName);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetIsReady(bool argbIsReady);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetIsAlive(bool argbIsAlive);

	UFUNCTION(NetMulticast, Reliable)
	void DestroyPlayerState();
protected:
	ASimonSaysPlayerController* OwningPlayerController;

	bool bHasArrivalBeenAnnounced = false;
};
