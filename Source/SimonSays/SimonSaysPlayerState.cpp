// Fill out your copyright notice in the Description page of Project Settings.

#include "SimonSaysPlayerState.h"
#include "UnrealNetwork.h"
#include "SimonSaysPlayerController.h"
#include "SimonSaysGameModeBase.h"



ASimonSaysPlayerState::ASimonSaysPlayerState()
{
	bReplicates = true;
}

//This is being utilized as our entry point to begin tracking PlayerState information in UI because unlike other entry point candidates, this one guarantees that the PlayerState object exists when it is called
void ASimonSaysPlayerState::OnRep_PlayerName()
{
	Super::OnRep_PlayerName();

	//Because this function is called whenever the PlayerName property is changed, we use a local boolean flag to ensure that we only treat this as our entry point the first time
 	if (!bHasArrivalBeenAnnounced)
 	{
 		bHasArrivalBeenAnnounced = true;
 
		//If we are not the server, we must add our own PlayerState here since this is the first place it is guaranteed to exist
		//The server adds theirs at a different time since it is the source of the replication
 		ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GetWorld()->GetFirstPlayerController());
 		if (SSPC != nullptr && ((!this->IsOwnedBy(SSPC) && Role == ROLE_Authority) || Role != ROLE_Authority))
 		{
 			SSPC->AddPlayerStatus(this);
 		}
 	}
}

void ASimonSaysPlayerState::OnDeactivated()
{
	if (Role == ROLE_Authority)
	{
		ASimonSaysGameModeBase* SSGM = Cast<ASimonSaysGameModeBase>(GetWorld()->GetAuthGameMode());
		SSGM->RemovePlayerState(this);
	}
}

void ASimonSaysPlayerState::DestroyPlayerState_Implementation()
{
	Destroy();
}

void ASimonSaysPlayerState::ServerSetIsAlive_Implementation(bool argbIsAlive)
{
	bIsAlive = argbIsAlive;
}

bool ASimonSaysPlayerState::ServerSetIsAlive_Validate(bool argbIsAlive)
{
	return true;
}

void ASimonSaysPlayerState::ServerSetIsReady_Implementation(bool argbIsReady)
{
	bIsReady = argbIsReady;

	ASimonSaysGameModeBase* SSGM =  Cast<ASimonSaysGameModeBase>(GetWorld()->GetAuthGameMode());
	//If we are the server this should be valid.  Also this function is a Server RPC so we are guaranteed to be the server here
	if (SSGM != nullptr)
	{
		SSGM->AdvanceIfAllPlayersReady();
	}
}

bool ASimonSaysPlayerState::ServerSetIsReady_Validate(bool argbIsReady)
{
	return true;
}

void ASimonSaysPlayerState::ServerSetPlayerName_Implementation(const FString& argName)
{
	SetPlayerName(argName);
}

bool ASimonSaysPlayerState::ServerSetPlayerName_Validate(const FString& argName)
{
	return true;
}

void ASimonSaysPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASimonSaysPlayerState, bIsReady);
	DOREPLIFETIME(ASimonSaysPlayerState, bIsAlive);
}