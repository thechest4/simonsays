// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SimonSaysPlayerController.generated.h"

class UReadyUpWidget;
class UPlayerStatusWidget;
class UPlayerElementTray;

UCLASS()
class SIMONSAYS_API ASimonSaysPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ASimonSaysPlayerController();

	UFUNCTION(Server, Reliable, WithValidation)
	void SubmitElementSelection(int argPatternIndex, int argElementId);

	UFUNCTION(Client, Reliable)
	void ShowPlayerReadyWidgets();

	UFUNCTION(Client, Reliable)
	void HidePlayerReadyWidget();

	UFUNCTION(Client, Reliable)
	void ShowPlayerStatusWidget();

	UFUNCTION(Client, Reliable)
	void ShowSurvivalPlayerStatus();

	UFUNCTION(Client, Reliable)
	void ShowReadyPlayerStatus();

	UFUNCTION(Client, Reliable)
	void UpdateCurrentPlayerStatus(int argCurrentPlayerIndex);

	UFUNCTION(Client, Reliable)
	void ShowPlayerElementTray();

	UFUNCTION(Client, Reliable)
	void HidePlayerElementTray();

	UFUNCTION(Client, Reliable)
	void HideAllWidgets();

	UFUNCTION(Server, Reliable, WithValidation)
	void ResetGame();

	UFUNCTION()
	void AddPlayerStatus(APlayerState* argPlayerState);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameUI")
	TSubclassOf<class UReadyUpWidget> ReadyUpWidgetClass;

	UPROPERTY()
	UReadyUpWidget* PlayerReadyWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameUI")
	TSubclassOf<class UPlayerStatusWidget> PlayerStatusWidgetClass;

	UPROPERTY()
	UPlayerStatusWidget* PlayerStatusWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameUI")
	TSubclassOf<class UPlayerElementTray> PlayerElementTrayClass;

	UPROPERTY()
	UPlayerElementTray* PlayerElementTray;
};
