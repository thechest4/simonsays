// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "SimonSaysGameState.generated.h"

class UPatternDisplayWidget;
class UGameOverWidget;
class UGameStateMessageWidget;
class UCountdownTimerWidget;

UCLASS()
class SIMONSAYS_API ASimonSaysGameState : public AGameState
{
	GENERATED_BODY()

public:
	UFUNCTION(NetMulticast, Reliable)
	void ShowPatternDisplayWidget();
	
	UFUNCTION(NetMulticast, Reliable)
	void HidePatternDisplayWidget();

	UFUNCTION(NetMulticast, Reliable)
	void AddPatternElement(int argElementId);

	UFUNCTION(NetMulticast, Reliable)
	void SetPatternElements(const TArray<int>& argElementIds);

	UFUNCTION(NetMulticast, Reliable)
	void ClearPattern();

	UFUNCTION(NetMulticast, Reliable)
	void ShowResultForSelection(int argPatternId, bool argbResult);

	UFUNCTION(NetMulticast, Reliable)
	void ResetAllResults();

	UFUNCTION(NetMulticast, Reliable)
	void ToggleAllElementsHidden(bool argbIsHidden);

	UFUNCTION(NetMulticast, Reliable)
	void ShowGameOverScreen(int argWinnerIndex);

	UFUNCTION(NetMulticast, Reliable)
	void HideGameOverScreen();

	UFUNCTION(NetMulticast, Reliable)
	void ShowCurrentTurnMessage(int CurrentPlayerStateId, float MessageDuration);

	UFUNCTION(NetMulticast, Reliable)
	void ShowTurnResultMessage(int CurrentPlayerStateId, bool bTurnWasSuccessful, float MessageDuration);

	UFUNCTION(NetMulticast, Reliable)
	void HideAllGameStateMessages();

	UFUNCTION(NetMulticast, Reliable)
	void ShowCountdownTimer(float argDuration);

protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<UPatternDisplayWidget> PatternDisplayWidgetClass;

	UPROPERTY()
	UPatternDisplayWidget* PatternDisplayWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameOverWidget> GameOverWidgetClass;

	UPROPERTY()
	UGameOverWidget* GameOverWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameStateMessageWidget> CurrentTurnMessageWidgetClass;

	UPROPERTY()
	UGameStateMessageWidget* CurrentTurnMessageWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameStateMessageWidget> TurnResultMessageWidgetClass;

	UPROPERTY()
	UGameStateMessageWidget* TurnResultMessageWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UCountdownTimerWidget> CountdownTimerWidgetClass;

	UPROPERTY()
	UCountdownTimerWidget* CountdownTimerWidget;
};
