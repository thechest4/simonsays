// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/GameState.h"
#include "GameFramework/PlayerState.h"
#include "SimonSaysGameModeBase.generated.h"

class ASimonSaysGameState;
class ASimonSaysPlayerController;
class UDataTable;

UCLASS()
class SIMONSAYS_API ASimonSaysGameModeBase : public AGameMode
{
	GENERATED_BODY()

	ASimonSaysGameModeBase();

public:
	virtual void PostLogin(APlayerController* NewPlayer) override;

	void AdvanceIfAllPlayersReady();

	void ReceivePlayerElementSubmission(int argPatternIndex, int argElementId);

	void ReloadCurrentLevel();

	void RestartGame();

	void RemovePlayerState(APlayerState* argPlayerStateToRemove);

protected:
	UPROPERTY(EditAnywhere)
	//After a player's turn has ended, how long (in seconds) do we wait before starting the next turn
	float DelayBetweenTurns = 3.0f;

	UPROPERTY(EditAnywhere)
	//After the pattern has been displayed at the start of a player's turn, how long (in seconds) until we hide the pattern and enable the player's Element Tray
	float ShowPatternDuration = 3.0f;

	UPROPERTY(EditAnywhere)
	//After the pattern has been displayed at the start of a player's turn, how long (in seconds) until we hide the pattern and enable the player's Element Tray
	float ShowPatternDurationSuddenDeath = 5.0f;

	float CurrentShowPatternDuration;

	UPROPERTY(EditAnywhere)
	//The max number of elements that can exist in a pattern.  If the pattern would exceed this number, instead randomize the rest of the pattern
	int PatternElementLimit = 10;

	UPROPERTY(EditAnywhere)
	UDataTable* ElementDataTable;

	TArray<int> CurrentPatternIds;

	ASimonSaysGameState* SSGameState;

	UPROPERTY(EditAnywhere)
	int RequiredPlayerCount = 2;

	int CurrentPlayerIndex;

	int CurrentPlayerStateId;

	void StartPlayForCurrentPlayer();

	void EndPlayForCurrentPlayer();

	int DeterminePlayerIndexToStart();

	void IncrementCurrentPlayerIndex();

	int GetRandomElementId();

	void AdvancePattern();

	UFUNCTION()
	void AdvanceToNextTurn(bool bIsFirstTurn, bool bIsDueToDisconnection);

	bool IsGameOver();

	void EndGame();

	void InitializePlayer(ASimonSaysPlayerController* PlayerToInitialize, bool bSetDefaultName);
};
