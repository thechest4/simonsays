// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerStatusRowWidget.h"
#include "GameFramework/PlayerState.h"
#include "SimonSaysPlayerState.h"




void UPlayerStatusRowWidget::BindPlayerState(APlayerState* argPlayerState)
{
	ASimonSaysPlayerState* SSPS = Cast<ASimonSaysPlayerState>(argPlayerState);
	if (SSPS != nullptr)
	{
		BoundPlayerState = SSPS;
		//If the PlayerState owner is the same as the owner of this widget, format this row to indicate that it is the local player's status
		//We actually need this null check here because on a remote client only one PlayerController is created, so PlayerState->GetOwner() will be null mroe often than not
		if (SSPS->GetOwner() != nullptr && SSPS->GetOwner() == GetOwningPlayer())
		{
			FormatForLocalPlayerRow();
		}
	}
}

void UPlayerStatusRowWidget::ToggleCurrentTurnIndicator(int argPlayerIndex)
{
	if (BoundPlayerState != nullptr)
	{
		SetCurrentTurnIndicatorVisibility(BoundPlayerState->PlayerId == argPlayerIndex);
	}
}

bool UPlayerStatusRowWidget::IsBoundPlayerStateValid()
{
	return (BoundPlayerState != nullptr && !BoundPlayerState->IsActorBeingDestroyed());
}

void UPlayerStatusRowWidget::FormatForLocalPlayerRow_Implementation()
{
	//Override in BP
}

void UPlayerStatusRowWidget::SetCurrentTurnIndicatorVisibility_Implementation(bool argVisible)
{
	//Override in BP
}

void UPlayerStatusRowWidget::SetGameAppearance_Implementation()
{
	//Override in BP
}

void UPlayerStatusRowWidget::SetPreGameAppearance_Implementation()
{
	//Override in BP
}
