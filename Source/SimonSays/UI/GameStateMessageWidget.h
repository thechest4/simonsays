// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameStateMessageWidget.generated.h"

class UTextBlock;


UCLASS()
class SIMONSAYS_API UGameStateMessageWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent)
	void InitGameStateMessageWidget();

	void DisplayMessage(const FString& MessageStr, float argDuration);

	void ShutDownWidget();

protected:
	UPROPERTY(BlueprintReadWrite)
	//This gets set in BP
	UTextBlock* TextWidget;
	
	
};
