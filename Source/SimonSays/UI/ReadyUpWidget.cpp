// Fill out your copyright notice in the Description page of Project Settings.

#include "ReadyUpWidget.h"
#include "GameFramework/GameStateBase.h"
#include "SimonSaysPlayerState.h"
#include "SimonSaysPlayerController.h"
#include "SimonSaysGameState.h"




void UReadyUpWidget::SetOwningPlayerController(ASimonSaysPlayerController* argPlayerController)
{
	OwningPlayerController = argPlayerController;
}

void UReadyUpWidget::SetPlayerName(FString argPlayerName)
{
 	if (OwningPlayerController != nullptr)
 	{
 		if (OwningPlayerState == nullptr)
		{
			//Since the PlayerState is not guaranteed to be available when we first call SetOwningPlayerController, try again to get a valid reference when we actually need it.
			//Since this function is driven by user interaction, it is probable that the PlayerState will be available long before the user actually has an opportunity to interact with the UI.
 			OwningPlayerState = Cast<ASimonSaysPlayerState>(OwningPlayerController->PlayerState);
 		}
 
 		if (OwningPlayerState != nullptr)
 		{
			OwningPlayerState->ServerSetPlayerName(argPlayerName);
 		}
 	}
}

void UReadyUpWidget::SetPlayerIsReady(bool argbIsReady)
{
	if (OwningPlayerController != nullptr)
	{
		if (OwningPlayerState == nullptr)
		{
			//Since the PlayerState is not guaranteed to be available when we first call SetOwningPlayerController, try again to get a valid reference when we actually need it.
			//Since this function is driven by user interaction, it is probable that the PlayerState will be available long before the user actually has an opportunity to interact with the UI.
			OwningPlayerState = Cast<ASimonSaysPlayerState>(OwningPlayerController->PlayerState);
		}

		if (OwningPlayerState != nullptr)
		{
			OwningPlayerState->ServerSetIsReady(argbIsReady);
		}
	}
}