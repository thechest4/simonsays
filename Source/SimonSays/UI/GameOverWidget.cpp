// Fill out your copyright notice in the Description page of Project Settings.

#include "GameOverWidget.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "SimonSaysPlayerController.h"




void UGameOverWidget::SetGameOverScreenText_Implementation(int argWinnerId)
{
	for (int i = 0; i < GetWorld()->GetGameState()->PlayerArray.Num(); i++)
	{
		//Found the winner
		if (GetWorld()->GetGameState()->PlayerArray[i]->PlayerId == argWinnerId)
		{	
			//Is it me?
			if (GetWorld()->GetGameState()->PlayerArray[i]->GetOwner() == GetOwningPlayer())
			{
				GameEndMessage = "You won!";
				break;
			}
			else
			{
				GameEndMessage = FString::Printf(TEXT("You lost!  %s won!"), *GetWorld()->GetGameState()->PlayerArray[i]->PlayerName);
				break;
			}
		}
	}

	//In BP override actually display the text on the widget
}

void UGameOverWidget::ResetGame()
{
	ASimonSaysPlayerController* SSPC = Cast<ASimonSaysPlayerController>(GetOwningPlayer());
	SSPC->ResetGame();
}
