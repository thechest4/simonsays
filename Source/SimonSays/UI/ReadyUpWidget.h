// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ReadyUpWidget.generated.h"

class ASimonSaysPlayerController;
class ASimonSaysPlayerState;
class ASimonSaysGameState;

UCLASS()
class SIMONSAYS_API UReadyUpWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void SetOwningPlayerController(ASimonSaysPlayerController* argPlayerController);
	
protected:
	ASimonSaysPlayerController* OwningPlayerController;

	ASimonSaysPlayerState* OwningPlayerState;

	ASimonSaysGameState* GameState;

	UFUNCTION(BlueprintCallable)
	void SetPlayerName(FString argPlayerName);
	
	UFUNCTION(BlueprintCallable)
	void SetPlayerIsReady(bool argbIsReady);
};
