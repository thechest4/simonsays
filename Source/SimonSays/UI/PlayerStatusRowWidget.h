// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerStatusRowWidget.generated.h"

class APlayerState;
class ASimonSaysPlayerState;

UCLASS()
class SIMONSAYS_API UPlayerStatusRowWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void BindPlayerState(APlayerState* argPlayerState);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetGameAppearance();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetPreGameAppearance();

	UFUNCTION()
	void ToggleCurrentTurnIndicator(int argPlayerIndex);

	UFUNCTION(BlueprintNativeEvent)
	void SetCurrentTurnIndicatorVisibility(bool argVisible);

	UFUNCTION()
	bool IsBoundPlayerStateValid();
	
protected:
	UPROPERTY(BlueprintReadWrite)
	ASimonSaysPlayerState* BoundPlayerState;
	
	UFUNCTION(BlueprintNativeEvent)
	void FormatForLocalPlayerRow();
};
