// Fill out your copyright notice in the Description page of Project Settings.

#include "CountdownTimerWidget.h"
#include "TextBlock.h"




void UCountdownTimerWidget::InitCountdownTimerWidget_Implementation()
{
	//Override in BP
}

void UCountdownTimerWidget::StartTimer(float argDuration)
{
	TimerEndTimestamp = GetWorld()->GetTimeSeconds() + argDuration;
	AddToViewport();
}

void UCountdownTimerWidget::ShutDownWidget()
{
	TimerEndTimestamp = 0.0f;
	RemoveFromParent();
}

void UCountdownTimerWidget::TickTimer()
{
	if (TextWidget != nullptr && TimerEndTimestamp > 0.0f)
	{
		float RemainingTime = TimerEndTimestamp - GetWorld()->GetTimeSeconds();

		if (RemainingTime < 0.0f)
		{
			ShutDownWidget();
		}

		FNumberFormattingOptions NumberFormat;
		NumberFormat.MinimumIntegralDigits = 1;
		NumberFormat.MaximumFractionalDigits = 2;
		NumberFormat.MaximumFractionalDigits = 2;
		TextWidget->SetText(FText::AsNumber(RemainingTime, &NumberFormat));
	}
}
