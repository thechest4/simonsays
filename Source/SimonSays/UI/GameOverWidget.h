// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"


UCLASS()
class SIMONSAYS_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent)
	void SetGameOverScreenText(int argWinnerId);
	
protected:
	UPROPERTY(BlueprintReadWrite)
	FString GameEndMessage;

	UFUNCTION(BlueprintCallable)
	void ResetGame();
};
