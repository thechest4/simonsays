// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuBase.h"




void UMainMenuBase::HostServer()
{
	FString TravelStr = GameMapName + "?listen";
	GetWorld()->ServerTravel(TravelStr);
}

void UMainMenuBase::JoinServer(FString argIpAddress)
{
	FString TravelStr = argIpAddress + ":7777";
	GetWorld()->GetFirstPlayerController()->ClientTravel(TravelStr, ETravelType::TRAVEL_Absolute);
}
