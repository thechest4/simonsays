// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerStatusWidget.h"
#include "GameFramework/PlayerState.h"
#include "VerticalBox.h"
#include "UI/PlayerStatusRowWidget.h"




void UPlayerStatusWidget::AddRow_Implementation(APlayerState* argPlayerState)
{
	//Blank implementation to be overridden in BP child
}

void UPlayerStatusWidget::InitPlayerStatusWidget_Implementation()
{
	//Override in BP
}

void UPlayerStatusWidget::ClearInvalidPlayerStatus()
{
	if (PlayerStatusContainer != nullptr)
	{
		//Since we'll be removing indices, reverse the for loop so that the iteration doesn't get messed up
		for (int i = PlayerStatusContainer->GetChildrenCount() - 1; i >= 0; i--)
		{
			UPlayerStatusRowWidget* Row = Cast<UPlayerStatusRowWidget>(PlayerStatusContainer->GetChildAt(i));
			if (Row != nullptr && !Row->IsBoundPlayerStateValid())
			{
				PlayerStatusContainer->RemoveChildAt(i);
			}
		}
	}
}

void UPlayerStatusWidget::ToggleCurrentPlayerIndicatorOnAllRows(int argCurrentPlayerIndex)
{
	if (PlayerStatusContainer != nullptr)
	{
		for (int i = 0; i < PlayerStatusContainer->GetChildrenCount(); i++)
		{
			UPlayerStatusRowWidget* Row = Cast<UPlayerStatusRowWidget>(PlayerStatusContainer->GetChildAt(i));
			Row->ToggleCurrentTurnIndicator(argCurrentPlayerIndex);
		}
	}
}

void UPlayerStatusWidget::SetPlayerStatusContainer(UVerticalBox* argPlayerStatusContainer)
{
	PlayerStatusContainer = argPlayerStatusContainer;
}

void UPlayerStatusWidget::SetGameAppearanceOnAllRows_Implementation()
{
	//Override in BP
}

void UPlayerStatusWidget::SetPreGameAppearanceOnAllRows_Implementation()
{
	//Override in BP
}