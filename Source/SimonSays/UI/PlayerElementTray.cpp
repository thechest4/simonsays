// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerElementTray.h"
#include "SimonSaysPlayerController.h"
#include "Engine/DataTable.h"
#include "PatternElement.h"
#include "HorizontalBox.h"
#include "HorizontalBoxSlot.h"
#include "PatternElementButton.h"
#include "Button.h"




void UPlayerElementTray::CreateAndAddElementButtons(UHorizontalBox* ElementWidgetContainer)
{
	FString ContextString;
	TArray<FPatternElementData*> PatternElements;
	ElementDataTable->GetAllRows(ContextString, PatternElements);

	for (int i = 0; i < PatternElements.Num(); i++)
	{
		if (ElementButtonClass != nullptr)
		{
			UPatternElementButton* PatternElementButton = CreateWidget<UPatternElementButton>(GetWorld(), ElementButtonClass);
			PatternElementButton->InitPatternElement(PatternElements[i]);
			PatternElementButton->OnElementSelected().AddUObject(this, &UPlayerElementTray::SelectElement);

			UHorizontalBoxSlot* HBoxSlot = ElementWidgetContainer->AddChildToHorizontalBox(PatternElementButton);
			HBoxSlot->SetHorizontalAlignment(HAlign_Fill);
			HBoxSlot->SetVerticalAlignment(VAlign_Fill);
			HBoxSlot->SetSize(FSlateChildSize(ESlateSizeRule::Fill));
			HBoxSlot->SetPadding(FMargin(10.0f));
		}
	}
}

void UPlayerElementTray::SetUpElements_Implementation()
{
	//Overridden in blueprint
}

void UPlayerElementTray::SetOwningPlayerController(ASimonSaysPlayerController* argPlayerController)
{
	OwningPlayerController = argPlayerController;
}

void UPlayerElementTray::ResetPatternIndex()
{
	CurrentPatternIndex = 0;
}

void UPlayerElementTray::SelectElement(int argElementId)
{
	if (OwningPlayerController != nullptr)
	{
		OwningPlayerController->SubmitElementSelection(CurrentPatternIndex, argElementId);
		CurrentPatternIndex++;
	}
}
