// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerStatusWidget.generated.h"

class UPlayerStatusRowWidget;
class APlayerState;
class UVerticalBox;

UCLASS()
class SIMONSAYS_API UPlayerStatusWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent)
	void InitPlayerStatusWidget();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void AddRow(APlayerState* argPlayerState);

	UFUNCTION(BlueprintNativeEvent)
	void SetGameAppearanceOnAllRows();

	UFUNCTION(BlueprintNativeEvent)
	void SetPreGameAppearanceOnAllRows();

	UFUNCTION()
	void ToggleCurrentPlayerIndicatorOnAllRows(int argCurrentPlayerIndex);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UPlayerStatusRowWidget> RowWidgetClass;

	UPROPERTY()
	UVerticalBox* PlayerStatusContainer;

	UFUNCTION(BlueprintCallable)
	void SetPlayerStatusContainer(UVerticalBox* argPlayerStatusContainer);

	UFUNCTION(BlueprintCallable)
	//We call this function on Tick, implemented in the BP because you can't override UserWidget Tick in C++
	void ClearInvalidPlayerStatus();
};
