// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PatternDisplayWidget.generated.h"

class UDataTable;
class UHorizontalBox;
class UPatternElementDisplay;


UCLASS()
class SIMONSAYS_API UPatternDisplayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void InitPatternDisplayWidget();

	void DisplayPattern(TArray<int> PatternElementIds);

	void AddToPattern(int ElementId);

	void ClearPattern();

	void ToggleAllElementsHidden(bool argbIsHidden);

	void ShowResultForElement(int argPatternId, bool argbResult);

	void ResetAllElementResults();
	
protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UPatternElementDisplay> ElementDisplayClass;

	UPROPERTY(EditAnywhere)
	UDataTable* ElementDataTable;

	UFUNCTION(BlueprintNativeEvent)
	UHorizontalBox* GetHorizontalBox();
	
	UHorizontalBox* HorizontalBox;

	FColor GetElementColor(int argElementId);
};
