// Fill out your copyright notice in the Description page of Project Settings.

#include "PatternElementDisplay.h"
#include "Image.h"




void UPatternElementDisplay::InitPatternElementDisplay(FColor argElementColor)
{
	ElementColor = argElementColor;

	ElementImage = GetElementImage();
	ResultImage = GetResultImage();

	ToggleElementColor(false);
	ToggleResultColor(true, false);
}

void UPatternElementDisplay::ToggleElementColor(bool argbIsHidden)
{
	if (ElementImage != nullptr)
	{
		FColor ColorToSet = (argbIsHidden) ? HiddenColor : ElementColor;
		ElementImage->SetColorAndOpacity(FLinearColor(ColorToSet));
	}
}

void UPatternElementDisplay::ToggleResultColor(bool argbIsHidden, bool argbResult)
{
	if (ResultImage != nullptr)
	{
		FColor ColorToSet = HiddenColor;

		if (!argbIsHidden)
		{
			ColorToSet = (argbResult) ? CorrectColor : WrongColor;
		}

		ResultImage->SetColorAndOpacity(FLinearColor(ColorToSet));
	}
}

void UPatternElementDisplay::SetElementColor(FColor argElementColor)
{
	ElementColor = argElementColor;
}

UImage* UPatternElementDisplay::GetResultImage_Implementation()
{
	//Implement in BP
	return nullptr;
}

UImage* UPatternElementDisplay::GetElementImage_Implementation()
{
	//Implement in BP
	return nullptr;
}
