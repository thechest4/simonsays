// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerElementTray.generated.h"

class ASimonSaysPlayerController;
class UDataTable;
class UPatternElementButton;
class UHorizontalBox;

UCLASS()
class SIMONSAYS_API UPlayerElementTray : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void SetUpElements();
	virtual void SetUpElements_Implementation();

	UFUNCTION(BlueprintCallable)
	void CreateAndAddElementButtons(UHorizontalBox* ElementWidgetContainer);

	UFUNCTION()
	void SetOwningPlayerController(ASimonSaysPlayerController* argPlayerController);

	void ResetPatternIndex();
	
protected:
	UPROPERTY(EditAnywhere)
	UDataTable* ElementDataTable;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UPatternElementButton> ElementButtonClass;

	UFUNCTION(BlueprintCallable)
	void SelectElement(int argElementId);

	ASimonSaysPlayerController* OwningPlayerController;

	int CurrentPatternIndex;
	
};
