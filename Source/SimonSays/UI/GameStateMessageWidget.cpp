// Fill out your copyright notice in the Description page of Project Settings.

#include "GameStateMessageWidget.h"
#include "TextBlock.h"




void UGameStateMessageWidget::InitGameStateMessageWidget_Implementation()
{
	//Override in BP
}

void UGameStateMessageWidget::DisplayMessage(const FString& MessageStr, float argDuration)
{
	if (TextWidget != nullptr)
	{
		TextWidget->SetText(FText::FromString(MessageStr));
		AddToViewport();

		if (argDuration > 0.0f)
		{
			FTimerHandle TimerHandle;
			GetOwningPlayer()->GetWorldTimerManager().SetTimer(TimerHandle, this, &UGameStateMessageWidget::RemoveFromParent, 1.0f, false, argDuration);
		}
	}
}

void UGameStateMessageWidget::ShutDownWidget()
{
	GetOwningPlayer()->GetWorldTimerManager().ClearAllTimersForObject(this);
	RemoveFromParent();
}
