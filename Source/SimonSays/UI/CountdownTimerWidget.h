// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CountdownTimerWidget.generated.h"

class UTextBlock;


UCLASS()
class SIMONSAYS_API UCountdownTimerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void InitCountdownTimerWidget();

	void StartTimer(float argDuration);

	void ShutDownWidget();

protected:
	UPROPERTY(BlueprintReadWrite)
	//This gets set in BP
	UTextBlock* TextWidget;

	float TimerEndTimestamp;
	
	UFUNCTION(BlueprintCallable)
	//Call this via the Blueprint Tick function
	void TickTimer();
	
};
