// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PatternElementDisplay.generated.h"

class UImage;

UCLASS()
class SIMONSAYS_API UPatternElementDisplay : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void InitPatternElementDisplay(FColor argElementColor);

	void ToggleElementColor(bool argbIsHidden);

	void ToggleResultColor(bool argbIsHidden, bool argbResult);

	void SetElementColor(FColor argElementColor);

protected:
	UPROPERTY(EditAnywhere)
	FColor ElementColor;

	UPROPERTY(EditAnywhere)
	FColor HiddenColor;

	UPROPERTY(EditAnywhere)
	FColor CorrectColor;

	UPROPERTY(EditAnywhere)
	FColor WrongColor;

	UFUNCTION(BlueprintNativeEvent)
	UImage* GetElementImage();

	UFUNCTION(BlueprintNativeEvent)
	UImage* GetResultImage();
	
	UImage* ElementImage;

	UImage* ResultImage;
};
