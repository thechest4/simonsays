// Fill out your copyright notice in the Description page of Project Settings.

#include "PatternDisplayWidget.h"
#include "Engine/DataTable.h"
#include "PatternElement.h"
#include "UI/PatternElementDisplay.h"
#include "HorizontalBox.h"
#include "HorizontalBoxSlot.h"




void UPatternDisplayWidget::InitPatternDisplayWidget()
{
	HorizontalBox = GetHorizontalBox();
}

//Clears the current pattern, and replace it with the passed in pattern
void UPatternDisplayWidget::DisplayPattern(TArray<int> PatternElementIds)
{
	ClearPattern();
	for (int i = 0; i < PatternElementIds.Num(); i++)
	{
		AddToPattern(PatternElementIds[i]);
	}
}

//Adds a new element to the end of the pattern
void UPatternDisplayWidget::AddToPattern(int ElementId)
{
	if (ElementDisplayClass != nullptr)
	{
		UPatternElementDisplay* PatternElementDisplay = CreateWidget<UPatternElementDisplay>(GetWorld(), ElementDisplayClass);
		PatternElementDisplay->InitPatternElementDisplay(GetElementColor(ElementId));

		UHorizontalBoxSlot* HBoxSlot = HorizontalBox->AddChildToHorizontalBox(PatternElementDisplay);
		HBoxSlot->SetHorizontalAlignment(HAlign_Fill);
		HBoxSlot->SetVerticalAlignment(VAlign_Fill);
		HBoxSlot->SetSize(FSlateChildSize(ESlateSizeRule::Automatic));
		HBoxSlot->SetPadding(FMargin(10.0f));
	}
}

void UPatternDisplayWidget::ClearPattern()
{
	HorizontalBox->ClearChildren();
}

void UPatternDisplayWidget::ToggleAllElementsHidden(bool argbIsHidden)
{
	for (int i = 0; i < HorizontalBox->GetChildrenCount(); i++)
	{
		UPatternElementDisplay* ElementDisplay = Cast<UPatternElementDisplay>(HorizontalBox->GetChildAt(i));
		//Hide all element colors
		ElementDisplay->ToggleElementColor(argbIsHidden);
	}
}

void UPatternDisplayWidget::ShowResultForElement(int argPatternId, bool argbResult)
{
	UPatternElementDisplay* ElementDisplay = Cast<UPatternElementDisplay>(HorizontalBox->GetChildAt(argPatternId));
	//Show the element color and the appropriate result color
	ElementDisplay->ToggleElementColor(false);
	ElementDisplay->ToggleResultColor(false, argbResult);
}

void UPatternDisplayWidget::ResetAllElementResults()
{
	for (int i = 0; i < HorizontalBox->GetChildrenCount(); i++)
	{
		UPatternElementDisplay* ElementDisplay = Cast<UPatternElementDisplay>(HorizontalBox->GetChildAt(i));
		//Hide all result colors.  Second parameter doesn't matter
		ElementDisplay->ToggleResultColor(true, false);
	}
}

FColor UPatternDisplayWidget::GetElementColor(int argElementId)
{
	FColor ToReturn = FColor::Black;

	FString ContextString;
	TArray<FPatternElementData*> PatternElements;
	ElementDataTable->GetAllRows(ContextString, PatternElements);

	for (int i = 0; i < PatternElements.Num(); i++)
	{
		if (PatternElements[i]->ElementId == argElementId)
		{
			ToReturn = PatternElements[i]->ElementColor;
			break;
		}
	}

	return ToReturn;
}

UHorizontalBox* UPatternDisplayWidget::GetHorizontalBox_Implementation()
{
	//Implement in BP
	return nullptr;
}
