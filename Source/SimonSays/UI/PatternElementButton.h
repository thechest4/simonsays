// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PatternElement.h"
#include "PatternElementButton.generated.h"

class UButton;

UCLASS()
class SIMONSAYS_API UPatternElementButton : public UUserWidget
{
	GENERATED_BODY()
	
public:
	int ElementId = -1;

	DECLARE_EVENT_OneParam(UPatternElementButton, ElementSelectedEvent, int)
	ElementSelectedEvent& OnElementSelected(){ return SelectedEvent; }

	void InitPatternElement(FPatternElementData* ElementData);

	UFUNCTION(BlueprintNativeEvent)
	UButton* GetButton();

	UFUNCTION()
	void BroadcastClickedEvent();
protected:

	ElementSelectedEvent SelectedEvent;
};
