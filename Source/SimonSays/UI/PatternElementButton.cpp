// Fill out your copyright notice in the Description page of Project Settings.

#include "PatternElementButton.h"
#include "Button.h"
#include "PatternElement.h"




void UPatternElementButton::InitPatternElement(FPatternElementData* ElementData)
{
	ElementId = ElementData->ElementId;

	UButton* ElementButton = GetButton();
	ElementButton->SetBackgroundColor(FLinearColor(ElementData->ElementColor));
	ElementButton->OnClicked.AddDynamic(this, &UPatternElementButton::BroadcastClickedEvent);
}


void UPatternElementButton::BroadcastClickedEvent()
{
	SelectedEvent.Broadcast(ElementId);
}

UButton* UPatternElementButton::GetButton_Implementation()
{
	//Override in BP
	return nullptr;
}
